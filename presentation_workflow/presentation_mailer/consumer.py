import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            email_address = data["presenter_email"]
            title = data["title"]
            send_mail(
                subject="Your presentation has been accepted",
                message=f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
                recipient_list=[email_address],
                from_email="admin@conference.go",
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            data = json.loads(body)
            presenter_name = data["presenter_name"]
            email_address = data["presenter_email"]
            title = data["title"]
            send_mail(
                subject="Your presentation has been rejected",
                message=f"{presenter_name}, we're happy to tell you that your presentation {title} has been rejected",
                recipient_list=[email_address],
                from_email="admin@conference.go",
                fail_silently=False,
            )

        # def main():
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejection")
        channel.basic_consume(
            queue="presentation_rejection",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)

# if __name__ == "__main__":
#     try:
#         main()
#     except KeyboardInterrupt:
#         print("Interrupted")
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
"/api/attendees/3/"
