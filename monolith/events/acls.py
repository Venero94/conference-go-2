from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    iso_country_code = "ISO 3166-2:US"
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{iso_country_code}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    geo_content = json.loads(geo_response.content)
    lat = geo_content[0]["lat"]
    lon = geo_content[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_content = json.loads(weather_response.content)
    weather_data = {
        "temp": weather_content["main"]["temp"],
        "description": weather_content["weather"][0]["description"],
    }
    try:
        return weather_data
    except (KeyError):
        return {"temp": None, "description": None}
